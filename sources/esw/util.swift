/**
 * Author: Matthieu Dorier
 * -------------------------------------------------------------------------
 * This Swift script provide utility functions for ESW.
 */

import python;
import files;
import io;

/**
 * Executes a Python script whose name is given as a string.
 */
run_python_script(string filename)
{
	python_file = input(filename);
	python_code = read(python_file);
	python(python_code);
}

/**
 * Tcl interface for the launch function allowing to start an MPI
 * application on some workers in parallel. 
 */
@par @dispatch=WORKER (int status) launch(string cmd, string args[])
  "launch" "0.0" "launch_tcl";

@par @dispatch=WORKER (int status) launch_turbine(string cmd, string args[])
  "launch" "0.0" "launch_turbine_tcl";
