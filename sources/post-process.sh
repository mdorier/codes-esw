#!/bin/bash

INSTANCE=$1
cd $INSTANCE
rm -rf *.pyc mpi-* results-* ross.csv tracer*
