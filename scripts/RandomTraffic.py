import cortex
import random
import yaml
import os

dst_list  = list()
initialized = False
seed = 0
num_msg = 0
msg_size = 0

def find_config():
  for f in os.listdir('.'):
    if f.endswith('.yml'):
      return f
  return None

def initialize():
  global dst_list, seed, num_msg, msg_size, initialized
  filename = find_config()
  if filename == None:
    raise RuntimeError("configuration file not found")
  yaml_file = open(filename)
  yaml_data = yaml_file.read()
  params = yaml.load(yaml_data)
  params = params['codes']['parameters']
  seed = params['seed']
  num_msg  = params['rd_traffic_num_msg']
  msg_size = params['rd_traffic_msg_size']

  random.seed(seed)
  ws = cortex.comm_world_size()
  for i in range(0,ws):
    for j in range(0,num_msg):
      k = random.randint(0,ws-1)
      while k == i:
        k = random.randint(0,ws-1)
      dst_list.append(k)
  
  initialized = True

def GenerateEvents(thread):
  global dst_list, num_msg, msg_size, initialized
  ws = cortex.comm_world_size()
  if(not initialized):
    initialize()
  for i in range(0,num_msg):
    dst = dst_list[i+thread*num_msg]
    cortex.MPI_Send(thread,count=msg_size,datatype=cortex.MPI_BYTE,dest=dst,tag=1234,comm=cortex.MPI_COMM_WORLD)
  for i in range(0,len(dst_list)):
    if dst_list[i] == thread:
       src = i / num_msg
       s = cortex.MPI_Status()
       cortex.MPI_Recv(thread,count=msg_size,datatype=cortex.MPI_BYTE,source=src,tag=1234,comm=cortex.MPI_COMM_WORLD,status=s)
    
