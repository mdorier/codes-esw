#!/bin/bash

set -e
set -o pipefail

STC=stc
export MPI=mpicc
export ENABLE_SHARED=1

echo "##### Creating build directory..."
mkdir build

echo "##### Copying source files into build directory..."
cp -r sources/* build/
cd build

echo "##### Downloading Swift Launch extention..."
git clone https://bitbucket.org/jmjwozniak/mpix_launch_swift.git
cd mpix_launch_swift
git checkout tags/valid-codes-esw-1
cd ..
mv mpix_launch_swift esw/launch

echo "##### Compiling Swift Launch extension..."
cd esw/launch/src
./build.sh
cd ../../..

echo "##### Compiling Swift scripts..."
$STC -I. -r $PWD/esw/launch main.swift
$STC -I. -r $PWD/esw/launch subworkflow.swift

echo "##### Done!"
