LPGROUPS
{
   MODELNET_GRP
   {
      repetitions="<<repetitions>>";
# name of this lp changes according to the model
      nw-lp="<<nw_lp>>";
# these lp names will be the same for dragonfly-custom model
      modelnet_dragonfly_custom="<<modelnet_dragonfly_custom>>";
      modelnet_dragonfly_custom_router="<<modelnet_dragonfly_custom_router:1>>";
   }
}
PARAMS
{
# packet size in the network 
   packet_size="<<packet_size:1024>>";
   modelnet_order=( "dragonfly_custom","dragonfly_custom_router" );
   # scheduler options
   modelnet_scheduler="fcfs";
# chunk size in the network (when chunk size = packet size, packets will not be
# divided into chunks)
   chunk_size="<<chunk_size:packet_size>>";
   # modelnet_scheduler="round-robin";
   # number of routers within each group
   # this is dictated by the dragonfly configuration files
   num_router_rows="<<num_router_rows:6>>";
   # number of router columns 
   num_router_cols="<<num_router_cols:16>>";
   # number of groups in the network
   num_groups="<<num_groups:9>>";
# buffer size in bytes for local virtual channels 
   local_vc_size="<<local_vc_size:8192>>";
#buffer size in bytes for global virtual channels 
   global_vc_size="<<global_vc_size:2*local_vc_size>>";
#buffer size in bytes for compute node virtual channels 
   cn_vc_size="<<cn_vc_size:local_vc_size>>";
#bandwidth in GiB/s for local channels 
   local_bandwidth="<<local_bandwidth:5.25>>";
# bandwidth in GiB/s for global channels 
   global_bandwidth="<<global_bandwidth:3.5*local_bandwidth>>";
# bandwidth in GiB/s for compute node-router channels 
   cn_bandwidth="<<cn_bandwidth:3*local_bandwidth>>";
# ROSS message size 
   message_size="<<message_size:592>>";
# number of compute nodes connected to router, dictated by dragonfly config
# file
   num_cns_per_router="<<num_cns_per_router:4>>";
# number of global channels per router 
   num_global_channels="<<num_global_channels:10>>";
# network config file for intra-group connections 
   intra-group-connections="<<intra_group_connections:'../../templates/dragonfly-intra-theta'>>";
# network config file for inter-group connections
   inter-group-connections="<<inter_group_connections:'../../templates/dragonfly-inter-theta'>>";
# routing protocol to be used 
   routing="<<routing:'adaptive'>>";
}
