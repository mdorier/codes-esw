LPGROUPS
{
   MODELNET_GRP
   {
      repetitions="<<repetitions>>";
      nw-lp="<<nw_lp:1>>";
      modelnet_torus="1";
   }
}
PARAMS
{
   packet_size="<<packet_size:512>>";
   message_size="<<message_size:592>>";
   modelnet_order=( "torus" );
   # scheduler options
   modelnet_scheduler="fcfs";
   net_startup_ns="<<net_startup_ns:1.5>>";
   net_bw_mbps="<<net_bw_mbps:20000>>";
   n_dims="<<n_dims:5>>";
   dim_length="<<dim_length:'8,8,4,4,4'>>";
   link_bandwidth="<<link_bandwidth:10.0>>";
   buffer_size="<<buffer_size:8192>>";
   chunk_size="<<chunk_size:packet_size>>";
}
